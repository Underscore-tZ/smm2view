#include <types.h>
#include <fstream>
#include <iostream>
#include <vector>

#include <nintendo/slope/Encrypted.hpp>

int main(int argc, const char* argv[]) {
  std::ifstream test_file("./tests/FTX1QFFHF.enc", std::ios::binary | std::ios::ate);
  const size_t test_size = test_file.tellg();

  test_file.seekg(0, std::ios::beg);

  std::vector<u8> test_data(test_size);
  test_file.read(reinterpret_cast<char*>(test_data.data()), test_size);
  test_file.close();

  auto course = nintendo::slope::Encrypted(test_data);

  return 0;
}
