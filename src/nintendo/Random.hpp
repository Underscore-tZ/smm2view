#pragma once
#include <types.h>
#include <array>

namespace nintendo {

class Random {
public:
  Random(u32 seed) { initContext(seed); };
  Random(std::array<u32, 4> context) : mContext(context) {};

  u32 getU32();
  u32 getU32(u32 max) { return getU32() * u64(max) >> 32; };

private:
  void initContext(u32 seed);
  void advanceContext(u32 value);

  std::array<u32, 4> mContext;
};

} // namespace nintendo

