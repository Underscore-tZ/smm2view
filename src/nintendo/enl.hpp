#pragma once
#include <types.h>
#include <array>
#include "Random.hpp"

namespace nintendo::enl {

typedef std::array<u32, 0x40> Context;

class Generator {
public:
  Generator(const Context& context, Random& random) : mContext(context), mRandom(random) {};
  std::array<u32, 4> generateKey();

private:
  const Context& mContext;
  Random& mRandom;
};

} // namespace nintendo::enl