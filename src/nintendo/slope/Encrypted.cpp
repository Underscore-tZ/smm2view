#include "Encrypted.hpp"
#include <nintendo/Random.hpp>
#include <nintendo/enl.hpp>
#include <tiny-AES-c/aes.hpp>
#include "slope.hpp"

namespace nintendo::slope {

bool Encrypted::parse(std::vector<u8> data) {
  const size_t decrypted_size = data.size() - 0x10 - 0x30;

  // create reader
  oishii::DataProvider dp(std::move(data));
  oishii::BinaryReader reader(dp.slice());
  reader.setEndian(std::endian::little);

  // read Header
  mVersion = reader.read<u32>();
  mType = reader.read<u16>();
  mUnk = reader.read<u16>(); // ???
  const u32 decrypted_crc = reader.read<u32>();
  reader.read<u32>(); // reserved

  // read Data
  reader.readBuffer(mData, decrypted_size);

  // read Footer
  mIV = reader.readX<u32, 4>();
  mRandomContext = reader.readX<u32, 4>();
  std::array<u32, 4> aes_cmac = reader.readX<u32, 4>();

  // decrypt data
  decrypt(aes_cmac);

  return false;
}

bool Encrypted::decrypt(std::array<u32, 4> cmac) {
  // generate key
  nintendo::Random random(mRandomContext);
  nintendo::enl::Generator generator(scCourseContext, random);
  auto aes_key = generator.generateKey();
  auto cmac_key = generator.generateKey();

  AES_ctx ctx;
  // TODO: does this work on big endian lol
  AES_init_ctx_iv(&ctx, reinterpret_cast<u8*>(aes_key.data()), reinterpret_cast<u8*>(mIV.data()));
  AES_CBC_decrypt_buffer(&ctx, mData.data(), mData.size());

  // TODO: crc and cmac checks

  return false;
}

} // namespace nintendo::slope