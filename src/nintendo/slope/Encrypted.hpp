#pragma once
#include <vector>
#include <array>
#include <oishii/reader/binary_reader.hxx>

namespace nintendo::slope {

class Encrypted {
public:
  Encrypted(std::vector<u8> data) { parse(data); };

  bool encrypt();

private:
  bool parse(std::vector<u8> data);
  bool decrypt(std::array<u32, 4> cmac);

  u32 mVersion;
  u16 mType;
  u16 mUnk;
  std::vector<u8> mData;

  std::array<u32, 4> mIV;
  std::array<u32, 4> mRandomContext;
};

} // namespace nintendo::slope