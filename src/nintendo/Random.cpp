#include "Random.hpp"

namespace nintendo {

void Random::initContext(u32 seed) {
  constexpr u32 multiplier = 0x6C078965;

  u32 value = seed;
  for (int i = 0; i < 4; ++i) {
    value ^= value >> 30;
    value *= multiplier + i;
    mContext[i] = value;
  }
}

void Random::advanceContext(u32 value) {
  mContext[0] = mContext[1];
  mContext[1] = mContext[2];
  mContext[2] = mContext[3];
  mContext[3] = value;
}

u32 Random::getU32() {
  u32 value = mContext[0];
  value ^= mContext[0] << 11;
  value ^= value >> 8;
  value ^= mContext[3];
  value ^= mContext[3] >> 19;

  advanceContext(value);
  return value;
}

} // namespace nintendo

