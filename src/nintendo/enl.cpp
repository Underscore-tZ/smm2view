#include "enl.hpp"
#include <iostream>

namespace nintendo::enl {

std::array<u32, 4> Generator::generateKey() {
  std::array<u32, 4> key;
  key[0] = 0;

  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      const u32 index = mRandom.getU32( mContext.size() );
      const u32 shift = mRandom.getU32(4) * 8;
      const u8 byte = mContext[index] >> shift;

      key[i] <<= 8;
      key[i] |= byte;
    }
  }

  return key;
}

} // namespace nintendo::enl